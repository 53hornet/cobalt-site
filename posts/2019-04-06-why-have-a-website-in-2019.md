---
permalink: /posts/{{categories}}/{{slug}}
title: Why Have a Website in 2019?
categories:
    - technology
tags:
    - website
    - self-hosted
    - blog
    - online presence
published_date: "2019-04-06 20:50:19 +0000"
layout: post.liquid
is_draft: false
---
> Adam, why on earth do you have a website? Wait... Is this a *blog*? It's
> 2019, why don't you just use Facebook?!

I've wanted to have my own website for a long time mostly because I like
playing with technology. I think it's cool that I can make a few files on a
computer in my closet available for the entire world to see. The web has become
a near-necessity in our daily lives and it's only been around for a couple of
decades. That has always fascinated me enough to drive me to see if I can do it
on my own. I started self-hosting my own web server about a year ago now and
it's been an awesome study in the way all of the tech we use on the web works.

A written website is also a good way to keep up on my writing skills. I've
graduated college and won't be writing essays for the foreseeable eternity.
Which is fine except that I have no reason to write creatively or formally
anymore. Having this site encourages me to continue turning thoughts into
words, even though it's more casual than a term paper. 

This extends into the professionalism of a personal website. Normally you can't
point your employer towards your Twitter profile as the distillation of your
online footprint. Having a website, especially one that separates professional
and personal interests, is ten times better than a resume, especially in the
world of technology. It lets you tell people exactly who you are and what you
do, without forcing them to navigate through an auth wall or a bunch of puppy
photos. Your professional and personal lives don't bleed together quite as much
and it makes for a kick-ass business card.

Most importantly however, I enjoy the level of control that I gain with
creating and operating my own website, something I have also sought for a long
time. In high school I set up an old Dell from my school's recycling center
with Windows XP and Microsoft Internet Information Services. It didn't have SSL
or even a domain name but it was reachable over the WAN. I set up an upload
system so that my friends and I could 'post' memes and funny messages for each
other on raw html pages written with Microsoft Word. That was the extent of its
functionality. It was slow, insecure, and went offline every time the router
got a new IP assigned to it. But I didn't care. It was a site the school
couldn't block. It had no name and no rules and nobody could tell us what to do
with it because it was ours. And we called it the *Troll Nexus Center* because
we were 15 and brimming with creativity.

My reasons then for building the Troll Nexus Center still stand now. *Having
your own website is having your own piece of internet property.* I first heard
this wording from [Luke Smith over on his YouTube
channel](https://www.youtube.com/watch?v=azkWYxyqh3Y) and it's one-hundred
percent true. Tumblogs, Google Sites, Facebook profiles, and GitHub Pages are
all like renting an apartment. Sure, there are some really nice apartments out
there ([we love ours!](https://www.thecommonwealthapts.com)) but it's not the
same as owning your own home. You have to pay rent obviously, and rent is
subject to change once your lease is up. If anything breaks you text your
landlord and wait to have it fixed. You aren't allowed to fix it yourself and
sometimes it doesn't get fixed at all. And of course you're limited by how much
you can customize things to your own liking. Whether it's painting walls or
knocking them down. 

These limitations may or may not apply to you. Whether you're paying for
storage, server space, metrics, or watching an ad every five seconds, these
services aren't free either. And you certainly can't fix everything that goes
wrong with them. I started on Google Sites. It's a truly fantastic system.
Building a site is like putting a PowerPoint slide together. I just plain
outgrew it. There were too many things I wanted to do that I simply couldn't. I
was also at the mercy of Google's constant change. After I finished
constructing my first site, Google [announced they would be shutting down the
old Google Sites in favor of an entirely new platform under the same
name](https://gsuiteupdates.googleblog.com/2016/11/a-totally-rebuilt-google-sitesnow.html).
Weeks of work got thrown out the window. You might also not care about ads or
customization. You may be intimidated by doing things yourself and prefer that
the landlord take care of everything. Personally, I like the challenge and the
craftsmanship that comes with doing something oneself. And I like being in
total control of my server, site, and content. Not from a tinfoil hat
perspective but from a "gosh I really wish I could just share more than 15
gigabytes of family video with my relatives in New York and Ohio" perspective.

So that's why I created my own website. If you want to know *how* I host my own
website, look for another post about my server setup where I'll explain
everything I'm hosting and how I got it all hooked up. And that's a wrap. Now
you know why I'm here instead of somewhere else online. Sure, I do have
Facebook and YouTube accounts but I don't frequently update anything on either
of them. This site is my home online. It's where I keep all of my interests,
hobbies, and memories for sharing with others. 

Now you know where to find me. If you want to keep up with me, subscribe to my
RSS feed up top! 

