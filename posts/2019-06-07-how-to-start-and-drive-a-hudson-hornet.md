---
permalink: "/posts/{{categories}}/{{slug}}"
title: How to Start and Drive a Hudson Hornet
categories:
  - automotive
tags:
  - hudson
  - hornet
  - start
  - drive
published_date: "2019-06-07 00:30:37 +0000"
layout: post.liquid
is_draft: false
---
My understanding is there are a lot of people out there for whom driving a car
from the mid-20th century is an oddity, a curiosity, or a life experience they
can't relate to. It's hard to capture what the actual experience is like in
words (hopefully my YouTube channel does a better job) but I think instead I
can do my best here to walk through the procedure of actually *operating* a
Hudson Hornet.

There are some significant differences between driving the Hornet and most
other cars you come across today. Some of them are just because there are
sixty-six years between the Hudson and the 2019 model year. Others are
Hudson-specific. Many people who I've talked to have said that they would feel
intimidated driving my car (whether that's because of its perceived complexity
or obvious value to me). So, for those who just want to know how it's done:
here is how you start and drive a Hudson Hornet.

# The Gauge Cluster, Switches, and Controls

Open the door, slide onto the bench seat, and sit behind the [massive] steering
wheel. For those who haven't experienced it before, it feels like you have a
whole lot of room at your disposal, almost like there *should* be more matter
occupying the space around you. In front of you is probably the shiniest
dashboard you've ever seen. It's simple, and probably slightly familiar. 

From left to right above the steering wheel you have: 

- A speedometer that tops out at 120 mph (with 99K odometer inside)
- Fuel and coolant temperature gauges (and two warning lights; more on those
  later)
- A mechanical, electrically-wound clock
- An AM radio
- A glove compartment

From left to right under the steering wheel you have: 

- A 2-speed wiper control knob
- A weather control (heater) temperature slider
- A 2-speed weather control fan knob
- The ignition barrel
- A headlight switch
- A cigar-lighter (yes, cigar. Check out the owner's manual)

Also, underneath the dashboard on the left there is a parking brake handle and
hood latch release and on the right there is an arm which raises and lowers the
fresh air cowl vent. Think of it as "recirculate" in more modern vehicles. If
you're looking for the turn signal lever it's the tiny stick to the left of the
steering wheel. The indicator is the little yellow light on the far left of the
dash. There's only one so it flashes when you're signalling left or right. We
also added our own air conditioning system, something Hudsons never came with
from the factory.

# Dual-Range Hydramatic

The first thing that might confuse some folks when they first see the car
running is the shift lever. Many Hornets came with three-speed manual
transmissions that were shifted from the column (overdrive was an option).
However, lots of owners paid extra for the optional "Dual-range Hydramatic", a
fully automatic transmission from General Motors. Truly, this car has a 4-speed
automatic that requires no manual shifting during normal use, making it that
much easier to take a boatload of people to get milkshakes.

Behind the steering wheel is a shift indicator that deviates from the "PRNDL"
pattern most folks are familiar with. From left to right (shift arm fully at
the top to arm fully towards the bottom), the 'gears' are:

- N (Neutral)
- 4-Dr (Drive, all four speeds)
- 3-Dr (Drive, three speeds only)
- Lo (Low gear)
- R (Reverse)

Neutral isn't just a mid-way point between reverse and drive in this car. It's
a necessity. With automatic Hornets (and Hydramatics in general), neutral is
used to start the car. There is an electric lockout preventing the car from
being started in any gear but neutral, so you do have to put the car in neutral
before you turn the key (if you're on a hill put your foot on the brake or
engage the parking brake).

Drive is split into 4-Dr and 3-Dr, which basically decides whether the
transmission utilizes high gear. In the owner's manual, Hudson recommends using
3-Dr for driving around town (as the low RPMs delivered by high gear means
unnecessary shifting in and out of 4th gear) and 4-Dr for highway driving. It
really depends on what speed you're going to be driving at but there isn't
anything wrong with driving around in 4 all the time. I typically leave it in
4th at sustained speeds above 45MPH. You can switch between these gears any
time while moving. 

Low gear basically locks the transmission in 2nd gear so you don't spin the
wheels. The owner's manual says this is for pulling out of sand or dirt if you
get stuck.

Reverse works just about how you might expect but with an added catch: if the
engine is off it acts as park. That's right. When you turn the car off you can
put it in reverse and the transmission will engage a lock pin to prevent the
car from rolling. You can't start the car in this gear because of the lockout
however so you have to shift into neutral to start the car. So for starting,
put it in neutral, for stopping, put it in reverse. 

# Choke and Gas

For cold starts, our Hornet (and I believe this was common for other Hudsons of
the time) is equipped with an automatic 2-stage choke. Push the pedal all the
way to the floor once to set the choke. After the car has started and has
warmed up, kick the gas quickly to the floor and release to cancel the choke. 

For warm starts the engine doesn't need the choke but likes to be given just a
little bit of gas while cranking.

# The Keys, Ignition, and Warning Lights

Hudsons like mine come with two keys. The octagonal one is for starting the
car, it's used in the ignition. The round one is used for the door and trunk
locks (and I believe in my case the glove box). My understanding is this is
actually reversed from the majority of Hudsons and is due to a locksmith error
at one point or another.

The ignition switch sits so that the teeth of the key enter vertically. Turning
the key left powers accessories like the radio. Turning the key right once
switches the car to "ON" which will allow the engine to be started and remain
running.

Here's where some things may vary depending on the year of the car. For '51
Hornets, there's a separate starter button located all the way on the left
control pod. For these cars, you put the key in and turn it to "ON", and then
press and hold the button until the car has started up. For '52 Hornets
onwards, the ignition switch also activates the starter if you turn the key
past "ON" (like in most modern vehicles). 

If you turn the key to "ON" you'll see two red warning lights appear on the
dash next to the indicators marked "AMP" and "OIL". These are [alternator]
charging status and oil pressure status lights. Our car is equipped with a 12-V
alternator system so the AMP light really comes on if there is low voltage
while the oil pressure light comes on when there's low oil pressure. These
lights will only appear with engine off, key "ON" or if something has gone very
wrong. 

# Starting and Driving

So now that I've gone over the basics of all the components, here is the normal
starting procedure. It actually varies depending on whether the engine has been
warmed up. That's life with carburetors.

## From cold: 

- Put your foot on the brake, and shift the lever into neutral. Just push it
  vertically, pulling towards you slightly if you need to.
- Push the gas pedal all the way to the floor once and let your foot back up
  again to set the choke. 
- Put the key in the ignition and start the car (the "AMP" and "OIL" lights
  should switch off. 
- Wait for the engine to smooth out so you know that it's warm enough to cancel
  the choke, and kick the gas pedal once to cancel it. (If the RPMs are still
higher than idle then it's not quite at operating temperature yet)
- Pull the shifter down into 4-Dr or 3-Dr (or R), and release the parking brake
  by twisting the handle towards the steering wheel
- Let off the brake and you're off!

## From warm:

- Put your foot on the brake and shift into neutral.
- While giving just a little bit of gas, start the car. Both warning lights
  should disappear. When the engine fires up you can let off the gas and let it
idle.
- Pull the shifter into 4-Dr or 3-Dr (or R), and release the parking brake by
  twisting the handle towards the steering wheel.
- Done.

# Stopping and parking

- Hold your foot on the brake and twist the parking brake handle towards the
  door of the car, and pull it towards you
- When you're ready to shut off the engine, you can shift it into either
  neutral or reverse and turn the key off. Shift it into reverse if you haven't
already to lock the transmission.

Note: I usually engage the parking brake AND put the car in reverse, just to be
safe. If you had to pick one however I would use the transmission in case
you're on a steep hill and your brakes fail for whatever reason.

And there you have it! Not much is different from most cars around today but
there are one or two quirks (more about old cars than about Hudsons in
particular). The only major thing to keep track of while driving is that you
have no power steering, so get ready to anticipate turns sooner and use more of
the wheel with every turn.
