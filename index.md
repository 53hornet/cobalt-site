---
layout: index.liquid
title: Welcome!
categories: ["index"]
---
# Site Index

Welcome! You've reached the personal and professional pages of Adam
Carpenter (53hornet). Below's a summary of what you'll find on my site.
If you want to search my site or get alerts when new content is added,
[subscribe to my RSS feed](/rss.xml).

## Journal

A running collection of posts about what I've been up to.

## More to Come

There's a lot more in the works as I continue to get this site set up the way I
want it. It's a work-in-progress currently but I've been enjoying the
transition from my older sites.

